//include <GOD>
#include <bits/stdc++.h>
#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(int J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<  long long , pii  >
#define 	pdd             		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).reeize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-7
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);
typedef long long ll;
typedef long double ld;
using namespace std;



//FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// const int sieve_size = 1000005;
// bool sieve[sieve_size+1];
//vll prime;
// void Sieve()
// {
//    prime.push_back(2);
//
//    for(int i = 3; i*i <=sieve_size ; i+=2){
//        if(!sieve[i]){
//            ll p = i*2;
//            for(int j = i*i ; j<= sieve_size ; j+=p){
//                sieve[j]=true;
//            }
//        }
//    }
//    for(int i = 3 ; i<=sieve_size ; i+=2){
//        if(!sieve[i]) prime.push_back(i);
//    }
// }
//
//

 int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
//int dx[] = {1,0,-1, 0 , 2 ,0 ,-2, 0 , 3 ,0 ,-3 ,0} ,
 //   dy[] = {0,1, 0, -1 , 0 ,2 ,0 , -2 , 0 , 3, 0 , -3};  // 4 Direction

 //int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
    return 0;
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}
    return 0;
}
const ll MD =  998244353;


bool isvalid(ll xx, ll yy , ll n , ll m ){
    return (xx>=0 && xx<n && yy>=0 && yy<m);
}

ll pw(ll x, ll y, ll dr){
    if(y==0) return 1;
    ll o = y/2;
    ll f = pw(x, o , dr)%dr;
    if(y%2){
        return (((f*f)%dr)*x)%dr;
    }
    else return (f*f)%dr;
}
  int values[8] = { 3, 5, 6, 9, 1, 2, 0, -1 };

ll ALPHABETA(ll alpha , ll beta , ll dep , ll nm){
    if(dep == 3) return values[nm];
    if(!(dep&1)){
        ll best = -inf ;
        Rep(i , 2){
            ll vl = ALPHABETA(alpha , beta , dep+1 , nm*2+i);
            best = max(best , vl);
            alpha = max(alpha , best);
            if(beta<=alpha){
                break;
            }
        }
        return best;
    }
    else{
            ll best = inf ;
        Rep(i , 2){
            ll vl = ALPHABETA(alpha , beta , dep+1 , nm*2+i);
            best = min(best , vl);
            beta = min(beta , best);
            if(beta<=alpha){
                break;
            }
        }
        return best;

    }
}
int main(){
        cout<<ALPHABETA(-inf , inf , 0,  0)<<endl;
        return 0 ;
}
