//include <GOD>
#include <bits/stdc++.h>
#define 	Time 	        	printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K)       	for(int J=R;J<K;++J)
#define 	Rep(I,N)         	For(I,0,N)
#define 	MP 		        	make_pair
#define 	ALL(X)       		(X).begin(),(X).end()
#define 	SF 	        		scanf
#define 	PF 		        	printf
#define 	pii         		pair<long long,long long>
#define 	piii         		pair<  long long , pii  >
#define 	pdd             		pair<double , double>
#define 	Sort(v) 	        sort(ALL(v))
#define     GSORT(x)            sort(ALL(x), greater<typeof(*((x).begin()))>())
#define 	Test 		        freopen("a.in","r",stdin)
#define 	Testout 	        freopen("a.out","w",stdout)
#define     UNIQUE(v)           Sort(v); (v).reeize(unique(ALL(v)) - (v).begin())
#define 	pb 		        	push_back
#define 	Set(a,n)        	memset(a,n,sizeof(a))
#define 	MAXN 		        100000+99
#define 	EPS 		        1e-7
#define 	inf 		        1ll<<61
#define     SF                  scanf
#define     PF                  printf
#define     F                   first
#define     S                   second
#define 	vpii         		vector<pii>
#define 	vll        		    vector<ll>
#define     booste              ios_base::sync_with_stdio(NULL); cin.tie(NULL); cout.tie(NULL);
typedef long long ll;
typedef long double ld;
using namespace std;



//FUNCTIONS
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// const int sieve_size = 1000005;
// bool sieve[sieve_size+1];
//vll prime;
// void Sieve()
// {
//    prime.push_back(2);
//
//    for(int i = 3; i*i <=sieve_size ; i+=2){
//        if(!sieve[i]){
//            ll p = i*2;
//            for(int j = i*i ; j<= sieve_size ; j+=p){
//                sieve[j]=true;
//            }
//        }
//    }
//    for(int i = 3 ; i<=sieve_size ; i+=2){
//        if(!sieve[i]) prime.push_back(i);
//    }
// }
//
//

 int dx[] = {1,0,-1, 0} , dy[] = {0,1, 0, -1};  // 4 Direction
//int dx[] = {1,0,-1, 0 , 2 ,0 ,-2, 0 , 3 ,0 ,-3 ,0} ,
 //   dy[] = {0,1, 0, -1 , 0 ,2 ,0 , -2 , 0 , 3, 0 , -3};  // 4 Direction

 //int dx[] = {1,-1,0,0,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1,1,-1}; // 8 Direction
/* int dx[] = {1,-1,1,-1,2,2,-2,-2} , dy[] = {2,2,-2,-2,1,-1,1,-1}; */ // Knight Direction
/* int dx[] = {2,-2,1,1,-1,-1} , dy[] = {0,0,1,-1,1,-1}; */ // Hexagonal Direction
inline int GA(ll n , vector<ll>&vec){
    Rep(i ,n){ll x ;cin>>x ; vec.push_back(x);}
    return 0;
}
inline int GAEG(ll m ,vector<ll>vec[]){
    Rep(i ,m){ll x  , y ;cin>>x>>y ; x-- , y-- ; vec[x].push_back(y); vec[y].push_back(x);}
    return 0;
}
const ll MD =  998244353;


bool isvalid(ll xx, ll yy , ll n , ll m ){
    return (xx>=0 && xx<n && yy>=0 && yy<m);
}

ll pw(ll x, ll y, ll dr){
    if(y==0) return 1;
    ll o = y/2;
    ll f = pw(x, o , dr)%dr;
    if(y%2){
        return (((f*f)%dr)*x)%dr;
    }
    else return (f*f)%dr;
}

ll has = 0 ;
string str[8]={
        "---K----",
        "-R----Q-",
        "--------",
        "-P----p-",
        "-----p--",
        "--------",
        "p---b--q",
        "----n--k"

};
ll ZobristTable[8][8][12];

mt19937 mt(time(0));

ll getRand(){
    uniform_int_distribution<ll> dist (0, inf);
    return dist(mt);
}
ll FillZobrist(){
    Rep(i , 8){
        Rep(j , 8){
            Rep(k ,12){
                ZobristTable[i][j][k] = getRand();
            }
        }
    }
}

ll getIndex(char ch){
     if (ch=='P')
        return 0;
    if (ch=='N')
        return 1;
    if (ch=='B')
        return 2;
    if (ch=='R')
        return 3;
    if (ch=='Q')
        return 4;
    if (ch=='K')
        return 5;
    if (ch=='p')
        return 6;
    if (ch=='n')
        return 7;
    if (ch=='b')
        return 8;
    if (ch=='r')
        return 9;
    if (ch=='q')
        return 10;
    if (ch=='k')
        return 11;
    else
        return -1;
}
int main(){
    FillZobrist();
    Rep(i ,8){
        Rep(j ,8){
            if(str[i][j] !='-') has^=ZobristTable[i][j][getIndex(str[i][j])];
        }
    }
    cout<<"First Hash Is : "<<endl;
    cout<<has<<endl;

    cout<<"Now we Change A element" <<endl;
    str[0][3] ='-';
    has^=ZobristTable[0][3]['K'];
    cout<<"New Hash Is"<<endl;
    cout<<has<<endl;
    str[0][3] ='K';
    has^=ZobristTable[0][3]['K'];
    cout<<"First Hash Is : "<<endl;
    cout<<has<<endl;

    return 0;
}
